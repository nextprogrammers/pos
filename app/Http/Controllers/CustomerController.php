<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CustomerRepository;

class CustomerController extends Controller
{
    private $customerRepo;
    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    public function Index()
    {
        return view('customer.index');
    }

    public function GetCustomers()
    {
        $customers = $this->customerRepo->Get();
        return response()->json(['customers' => $customers]);
    }
    
    public function AddCustomer(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'previous_due' => 'required'
        ]);
        
        $res = $this->customerRepo->Insert($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdateCustomer(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'previous_due' => 'required'
        ]);
        
        $res = $this->customerRepo->Update($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function DeleteCustomer(Request $request)
    {
        $id = $request->id;
        $res = $this->customerRepo->Delete($id);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function GetCustomerNewCode()
    {
        $customerCode = $this->customerRepo->NewCustomerCode();
        return response()->json(['code' => $customerCode]);
    }
}

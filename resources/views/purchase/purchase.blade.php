@extends('master')
@section('title', 'Purchase Product')
@section('breadcrumb', 'Purchase Product')
@section('page_name', 'Purchases Product')
@section("custom_css")
<link href="assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen">
@endsection
@section('content')
<div id="app">
    <purchase-component></purchase-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


<?php

namespace App\Http\Controllers;

use App\ProductDetail;
use App\ProductTransaction;
use App\Repositories\SaleRepository;
use App\Sale;
use App\SaleDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class SaleController extends Controller
{
    private $saleRepo;

    public function __construct(SaleRepository $saleRepo)
    {
      $this->saleRepo = $saleRepo;
    }


    public function Index()
    {
        return view('sale.index');
    }

    public function GetSaleInvoice()
    {
        $invoice = $this->saleRepo->NewSaleInvoice();
        return response()->json(['invoice' => $invoice]);
    }

    public function SaveSale(Request $request)
    {
        $res = new stdClass();
        try{

            DB::transaction(function () use($request) {

                $invoice = $this->saleRepo->NewSaleInvoice();

                $sales = $request->post("sale");
                // sales master
                $sale = new Sale();
                $sale->sale_type = $request->saleType;
                $sale->customer_id = $sales['customer_id'];
                $sale->entry_date = $sales['entry_date'];
                $sale->invoice = $invoice;
                $sale->sub_total = $sales['sub_total'];
                $sale->discount_percent = $sales['discount_percent'];
                $sale->tax_percent = $sales['tax_percent'];
                $sale->vat_percent = $sales['vat_percent'];
                $sale->other_cost = $sales['other_cost'];
                $sale->total = $sales['total'];
                $sale->paid = $sales['paid'];
                $sale->payment_type = $sales['payment_type'];
                $sale->transaction_number = $sales['transaction_number'];
                $sale->note = $sales['note'];
                $sale->branch_id = app('BRANCHID');
                $sale->created_by = app('USERID');
                $sale->save();

                $saleLastId = $sale->id;

                // sales details
                
                $saleDetails = $request->post('cart');

                foreach($saleDetails as $detail){
                    
                    $saleDetail = new SaleDetails();
                    $saleDetail->sale_id = $saleLastId;
                    $saleDetail->product_detail_id = $detail['product_detail_id'];
                    $saleDetail->amount = $request->saleType == 'Retail' ? $detail['sale_rate'] : $detail['wholesale_rate'];
                    $saleDetail->discount_percent = $detail['discount_percent'];
                    $saleDetail->tax_percent = $detail['tax_percent'];
                    $saleDetail->entry_date = $sales['entry_date'];
                    $saleDetail->qty = $detail['qty'];
                    $saleDetail->branch_id = app('BRANCHID');
                    $saleDetail->created_by = app('USERID');
                    $saleDetail->save();

                    // product transaction
                    $transaction = new ProductTransaction();
                    $transaction->invoice_id =  $saleLastId;
                    $transaction->product_detail_id =  $detail['product_detail_id'];
                    $transaction->transaction_type = 'sale';
                    $transaction->qty = (-$detail['qty']);
                    $transaction->branch_id = app('BRANCHID');
                    $transaction->created_by = app('USERID');
                    $transaction->save();
                }

            }); #END transaction

            $res->message = 'Product was sale';
            $res->status = 200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

    public function GetSaleById(Request $request)
    {
        $saleId = $request->id;
        $sale = Sale::with(['Customer','SaleDetails','SaleDetails.ProductDetail','SaleDetails.ProductDetail.Product'])
                ->where('id', $saleId)
                ->where('branch_id', app('BRANCHID'))
                ->get();

        return response()->json(['sale' => $sale], 200);
    }

    public function DeleteSaleDetail(Request $request)
    {
        $res = new stdClass();
        try{
            DB::transaction(function () use($request) {
                // remove purchase details
                SaleDetails::where('id', $request->sale_detail_id)
                ->delete();

                // remove product transaction
                ProductTransaction::where('invoice_id', $request->sale_id)
                ->where('product_detail_id', $request->product_detail_id)
                ->delete();

                // remove product details if this not use other purchase details
                $countProduct = SaleDetails::where('active', true)
                        ->where('sale_id','!=', $request->sale_id)
                        ->where('product_detail_id', $request->product_detail_id)
                        ->count();
                if($countProduct == 0){
                    SaleDetails::where('id', $request->product_detail_id)->delete();
                } 
                
                // update purchase
                $purchase = Sale::find($request->sale_id);
                $purchase->sub_total -= $request->minus_amount;
                $purchase->save();
                
            }); #END transaction

            $res->message = 'Product remove successfully';
            $res->status = 200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdateSale(Request $request)
    {
        $res = new stdClass();

        try{
            DB::transaction(function () use($request) {

                // save purchase
                $sale = $request->post('sale');
                DB::table('sales')
                    ->where('id', $sale['id'])
                    ->update([
                    'customer_id' => $sale['customer_id'],
                    'entry_date' => $sale['entry_date'],
                    'sub_total' => $sale['sub_total'],
                    'sale_type' => $request->saleType,
                    'discount_percent' => $sale['discount_percent'],
                    'tax_percent' => $sale['tax_percent'],
                    'vat_percent' => $sale['vat_percent'],
                    'other_cost' => $sale['other_cost'],
                    'total' => $sale['total'],
                    'payment_type' => $sale['payment_type'],
                    'transaction_number' => $sale['transaction_number'],
                    'paid' => $sale['paid'],
                    'note' => $sale['note'],
                    'updated_by' => app('USERID')
                ]);

                // purchase details table update
                $saleDetails = $request->post('cart');
                foreach($saleDetails as $cart){
                    
                    // when change field value 
                    if($cart['id'] > 0){
                        
                        // update purchase details table
                        $details = SaleDetails::find($cart['id']);
                        $details->sale_id = $sale['id'];
                        $details->entry_date = $sale['entry_date'];
                        $details->qty = $cart['qty'];
                        $details->product_detail_id = $cart['product_detail_id'];
                        $details->updated_by = app('USERID');
                        $details->save();

                        // update transaction table
                        $transaction = ProductTransaction::where('invoice_id', $cart['sale_id'])
                                        ->where('product_detail_id', $cart['product_detail_id'])
                                        ->update([
                                            'qty' => $cart['qty'],
                                            'updated_by' => app('USERID')
                                        ]);

                    }

                    // this product not exists in purchase details table
                    if($cart['id'] == 0){

                        // save purchase details
                        $saleDetail = new SaleDetails();
                        $saleDetail->purchase_id = $sale['id'];
                        $saleDetail->entry_date = $sale['entry_date'];
                        $saleDetail->qty = $cart['qty'];
                        $saleDetail->product_detail_id = $cart['product_detail_id'];
                        $saleDetail->branch_id = app('BRANCHID');
                        $saleDetail->created_by = app('USERID');
                        $saleDetail->save();

                        // save product transaction

                        $transaction = new ProductTransaction();
                        $transaction->invoice_id =  $sale['id'];
                        $transaction->product_detail_id = $cart['product_detail_id'];
                        $transaction->transaction_type = 'sale';
                        $transaction->qty = $cart['qty'];
                        $transaction->branch_id = app('BRANCHID');
                        $transaction->created_by = app('USERID');
                        $transaction->save();

                    }

                }
                // end purchase details

            }); #END transaction

            $res->message = 'Sale was update';
            $res->status = 200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

    public function GetSaleReportWithoutDetails(Request $request)
    {

        $query = Sale::where('active', true)
                ->where('branch_id', app('BRANCHID'));
        
            if(isset($request->from_date) && isset($request->to_date) && $request->invoice == null){
                $query->whereBetween('entry_date', [$request->from_date, $request->to_date]);
            }

            if(isset($request->customer_id) && $request->customer_id != null){
                $query->where('customer_id', $request->customer_id);
            }

            if(isset($request->invoice) && $request->invoice != null){
                $query->where('invoice', $request->invoice);
            }

        $sales = $query->get();
        return response()->json(['sales' => $sales]);
    }

    public function GetSaleReportWithDetails(Request $request)
    {
        
        $query = Sale::with(['Customer','SaleDetails','SaleDetails.ProductDetail','SaleDetails.ProductDetail.Product'])
                ->where('active', true)
                ->where('branch_id', app('BRANCHID'));
        
            if(isset($request->from_date) && isset($request->to_date) && $request->invoice == null){
                $query->whereBetween('entry_date', [$request->from_date, $request->to_date]);
            }

            if(isset($request->customer_id) && $request->customer_id != null){
                $query->where('customer_id', $request->customer_id);
            }

            if(isset($request->invoice) && $request->invoice != null){
                $query->where('invoice', $request->invoice);
            }

        $sales = $query->get();
        return response()->json(['sales' => $sales]);
    }

    public function GetCustomerWishInvoice(Request $request)
    {
        $invoices = Sale::select('invoice')
                    ->where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->where('customer_id', $request->customer_id)
                    ->get();

        return response()->json(['invoices' => $invoices]);
    }

    public function GetAllSaleInvoices()
    {
        $invoices = Sale::select('invoice')
                    ->where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->get();

        return response()->json(['invoices' => $invoices]);
    }

}

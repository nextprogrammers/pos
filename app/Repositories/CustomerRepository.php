<?php
namespace App\Repositories;

use App\Customer;
use stdClass;

class CustomerRepository {

    public function Get()
    {
        $customers = Customer::where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->get()
                    ->map(function($customer){
                        return[
                            'id' => $customer->id,
                            'name' => $customer->name,
                            'code' => $customer->code,
                            'phone' => $customer->phone,
                            'email' => $customer->email,
                            'address' => $customer->address,
                            'previous_due' => $customer->previous_due,
                            'display_name' => $customer->name.'-'.$customer->phone.' ( '.$customer->code.' )'
                        ];
                    });

        return $customers;
    }

    public function GetById($customerId)
    {
        $customer = Customer::where('id', $customerId)
                    ->where('branch_id', app('BRANCHID'))
                    ->first();

        return $customer;
    }


    public function Insert($request)
    {
        $res = new stdClass();
        try{
            $customer = new Customer();
            $customer->name = $request->name;
            $customer->code = $this->NewCustomerCode();
            $customer->phone = $request->phone;
            $customer->email = $request->email;
            $customer->previous_due = $request->previous_due;
            $customer->address = $request->address;
            $customer->active = true;
            $customer->created_by = app('USERID'); 
            $customer->branch_id = app('BRANCHID');
            $customer->save();

            $res->message = 'Customer was add';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function Delete($customerId)
    {
        $res = new stdClass();
        try{
            $customer = $this->GetById($customerId);
            $customer->active = false;
            $customer->save();
            $res->message = "Customer was delete";
            $res->status = 200;
        }catch(Exception $ex){
            $res->message = $ex->getMessage();
            $res->status = $ex->getCode();
        }

        return $res;
    }

    public function Update($request)
    {
        $res = new stdClass();
        try{
            $customer = Customer::find($request->id);
            $customer->name = $request->name;
            $customer->phone = $request->phone;
            $customer->email = $request->email;
            $customer->previous_due = $request->previous_due;
            $customer->address = $request->address;
            $customer->save();

            $res->message = 'Customer was update';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function NewCustomerCode()
    {
        $customer = Customer::orderBy('id', 'desc');
        $customerId = ($customer->count() == 0 ? 0 : $customer->first()->id) + 1;
        $zeros = 5 - strlen($customerId);
        $newCustomerCode = "C";
        for($i = 1; $i <= $zeros; $i++) $newCustomerCode .= '0';
        $newCustomerCode .= $customerId;
        return $newCustomerCode;
    }
}
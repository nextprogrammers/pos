@extends('master')
@section('title', 'Purchase')
@section('breadcrumb', 'Purchase Module')
@section('page_name', 'Purchase Module')
@section('content')
<div class="row">
    <div class="col-md-2 mt-3 ml-0">
        <div class="desktop_icon">
            <a href="{{url('customers')}}">
                <span class="mdi mdi-cube-send"></span>
                <p>Customers</p>
            </a>
        </div>
    </div>

    <div class="col-md-2 mt-3 ml-0">
        <div class="desktop_icon">
            <a href="{{url('suppliers')}}">
                <span class="mdi mdi-cube-send"></span>
                <p>Suppliers</p>
            </a>
        </div>
    </div>

    <div class="col-md-2 mt-3 ml-0">
        <div class="desktop_icon">
            <a href="{{url('purchase')}}">
                <span class="mdi mdi-shopping"></span>
                <p>Purchase</p>
            </a>
        </div>
    </div>

</div>
@endsection

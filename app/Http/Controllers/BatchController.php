<?php

namespace App\Http\Controllers;

use App\Batch;
use App\ProductDetail;
use Illuminate\Http\Request;
use App\Repositories\BatchRepository;

class BatchController extends Controller
{
    private $batchRepo;
    public function __construct(BatchRepository $batchRepo)
    {
        $this->batchRepo = $batchRepo;
    }

    public function Index()
    {
        return view('batch.index');
    }

    public function GetBatches()
    {
        $batches = $this->batchRepo->Get();
        return response()->json(['batches' => $batches]);
    }
    
    public function AddBatch(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);
        
        $res = $this->batchRepo->Insert($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdateBatch(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);
        
        $res = $this->batchRepo->Update($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function DeleteBatch(Request $request)
    {
        $id = $request->id;
        $res = $this->batchRepo->Delete($id);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function GetBatchNewCode()
    {
        $batchCode = $this->batchRepo->NewBatchCode();
        return response()->json(['code' => $batchCode]);
    }

    public function GetBatchesByProduct(Request $request)
    {
        // get 
        $productId = ProductDetail::select('batch_id')->where('product_id', $request->product_id)->get();

        $batches = Batch::whereIn('id', $productId)->get();
        return response()->json(['batches' => $batches]);
    }
}

<?php
namespace App\Repositories;

use App\Purchase;
use stdClass;

class PurchaseRepository {

    public function NewPurchaseInvoice() 
    {
        $purchase = Purchase::orderBy('id', 'desc');
        $purchaseId = ($purchase->count() == 0 ? 0 : $purchase->first()->id) + 1;
        $zeros = 5 - strlen($purchaseId);
        $newProductCode = "PUR";
        for($i = 1; $i <= $zeros; $i++) $newProductCode .= '0';
        $newProductCode .= $purchaseId;
        return $newProductCode;
    }
}
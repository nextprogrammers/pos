<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SupplierRepository;

class SupplierController extends Controller
{
    private $supplierRepo;
    public function __construct(SupplierRepository $supplierRepo)
    {
        $this->supplierRepo = $supplierRepo;
    }

    public function Index()
    {
        return view('supplier.index');
    }

    public function GetSuppliers()
    {
        $suppliers = $this->supplierRepo->Get();
        return response()->json(['suppliers' => $suppliers]);
    }
    
    public function AddSupplier(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'previous_due' => 'required'
        ]);
        
        $res = $this->supplierRepo->Insert($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdateSupplier(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|max:255',
        //     'previous_due' => 'required'
        // ]);
        
        $res = $this->supplierRepo->Update($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function DeleteSupplier(Request $request)
    {
        $id = $request->id;
        $res = $this->supplierRepo->Delete($id);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function GetSupplierNewCode()
    {
        $supplierCode = $this->supplierRepo->NewSupplierCode();
        return response()->json(['code' => $supplierCode]);
    }
}

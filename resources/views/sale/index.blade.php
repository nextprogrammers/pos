@extends('master')
@section('title', 'Sale Entry')
@section('breadcrumb', 'Sale Entry')
@section('page_name', 'Manage Sales')
@section("custom_css")
<link href="assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen">
@endsection
@section('content')
<div id="app">
    <sale-component></sale-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseReturnDetail;
use App\ProductTransaction;
use stdClass;

class PurchaseReturnController extends Controller
{
    public function Index()
    {
        return view('purchase.purchase_return');
    }
    
    public function PurchaseReturn(Request $request)
    {
        $res = new stdClass();
        
        try{
            DB::transaction(function () {
                // purchase return master

                $purchaseReturn = $this->request->post('purchaseReturn');

                DB::table('purchase_returns')->insert([
                    'entry_date' => $purchaseReturn->entry_date,
                    'purchase_id' => $purchaseReturn->purchase_id,
                    'qty' => $purchaseReturn->qty,
                    'amount' => $purchaseReturn->amount,
                    'branch_id' => app('BRANCHID'),
                    'created_by' => app('USERID')
                ]);

                $lastPurchaseReturnId = DB::getPdo()->lastInsertId();

                foreach($purchaseReturn->purchase_details as $cart){

                    if($cart->return_qty > 0){

                    $purchaseReturnDetail = new PurchaseReturnDetail();

                    $purchaseReturnDetail->purchase_return_id = $lastPurchaseReturnId;
                    $purchaseReturnDetail->product_detail_id = $cart->product_detail_id;
                    $purchaseReturnDetail->qty = $cart->purchase_return_qty;
                    $purchaseReturnDetail->branch_id = app('BRANCHID');
                    $purchaseReturnDetail->created_by = app('USERID');
                    $purchaseReturnDetail->save();

                    }
                    
                }

                // save product transaction

                $transaction = new ProductTransaction();
                $transaction->invoice_id =  $lastPurchaseReturnId;
                $transaction->transaction_type = 'purchase_return';
                $transaction->qty = $purchaseReturn->qty;
                $transaction->branch_id = app('BRANCHID');
                $transaction->created_by = app('USERID');
                $transaction->save();

            }); #END transaction

            $res->message = "Purchase return was success";
            $res->status =  200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

}

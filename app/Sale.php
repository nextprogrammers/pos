<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function Customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function SaleDetails()
    {
        return $this->hasMany('App\SaleDetails');
    }
}

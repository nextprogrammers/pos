@extends('master')
@section('title', 'Product')
@section('breadcrumb', 'Product')
@section('page_name', 'Product Module')
@section('content')
<div class="row">
    <div class="col-md-2 mt-3 ml-0">
        <div class="desktop_icon">
            <a href="{{url('products')}}">
                <span class="mdi mdi-cube-send"></span>
                <p>Products</p>
            </a>
        </div>
    </div>

    <div class="col-md-2 mt-3 ml-0">
        <div class="desktop_icon">
            <a href="{{url('units')}}">
                <span class="mdi mdi-cube-send"></span>
                <p>Units</p>
            </a>
        </div>
    </div>

</div>
@endsection

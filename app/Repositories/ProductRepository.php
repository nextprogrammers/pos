<?php
namespace App\Repositories;

use App\Product;
use stdClass;

class ProductRepository {

    public function Get()
    {
        $products = Product::with('Unit')->where('active', true)->get();
        return $products;
    }

    public function InsertProduct($request)
    {
        $res = new stdClass();
        try{
            $product = new Product();
            $product->code = $this->NewProductCode();
            $product->name = $request->name;
            $product->unit_id = $request->unit_id;
            $product->branch_id = app('BRANCHID');
            $product->save();

            $res->message = 'Product was add';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function Update($request)
    {
        $res = new stdClass();
        try{
            $product = Product::find($request->id);
            $product->name = $request->name;
            $product->unit_id = $request->unit_id;
            $product->save();

            $res->message = 'Product was update';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function Delete($id)
    {
        $res = new stdClass();
        try{
            $product = Product::find($id);
            $product->active = false;
            $product->save();
            
            $res->message = 'Product was delete';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function NewProductCode() 
    {
        $product = Product::orderBy('id', 'desc');
        $productId = ($product->count() == 0 ? 0 : $product->first()->id) + 1;
        $zeros = 5 - strlen($productId);
        $newProductCode = "P";
        for($i = 1; $i <= $zeros; $i++) $newProductCode .= '0';
        $newProductCode .= $productId;
        return $newProductCode;
    }

}
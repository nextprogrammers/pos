<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductDetail;
use App\Purchase;
use App\PurchaseDetail;
use App\ProductTransaction;
use App\Exception\Handler;
use App\Repositories\PurchaseRepository;
use Illuminate\Support\Facades\DB;
use stdClass;

class PurchaseController extends Controller
{
    private $purchaseRepo;

    public function __construct(PurchaseRepository $purchaseRepo)
    {
      $this->purchaseRepo = $purchaseRepo;
    }

    public function Index()
    {
        return view('purchase.index');
    }
    
    public function Purchase()
    {
        return view('purchase.purchase');
    }

    public function GetPurchaseById(Request $request){
        $purchaseId = $request->input('purchase_id');

        $purchase = Purchase::with(['Supplier','PurchaseDetails.ProductDetail.Product.Unit','PurchaseDetails.ProductDetail.Batch', 'ProductTransactions'])
                    ->where('id', $purchaseId)
                    ->where('branch_id', app('BRANCHID'))
                    ->get();
                    
        return response()->json(['purchase' => $purchase], 200);
    }

    public function PurchasePlace(Request $request)
    {
        $res = new stdClass();
        try{
            DB::transaction(function () use($request) {

                // save purchase
                $purchase = (object)$request->post('purchase_details');
                DB::table('purchases')->insert([
                    'invoice' => $this->purchaseRepo->NewPurchaseInvoice(),
                    'entry_date' => $purchase->entry_date,
                    'supplier_id' => $purchase->supplier_id,
                    'sub_total' => $purchase->sub_total,
                    'discount_percent' => $purchase->discount_percent,
                    'vat_percent' => $purchase->vat_percent,
                    'transport_cost' => $purchase->transport_cost,
                    'other_cost' => $purchase->other_cost,
                    'total' => $purchase->total,
                    'payment_type' => $purchase->payment_type,
                    'transaction_number' => $purchase->transaction_number,
                    'paid' => $purchase->paid,
                    'previous_due' => $purchase->previous_due,
                    'note' => $purchase->note,
                    'branch_id' => app('BRANCHID'),
                    'created_by' => app('USERID')
                ]);

                $lastPurchaseId = DB::getPdo()->lastInsertId();

                $purchaseDetails =  $request->post('cart');
                foreach($purchaseDetails as $cart){
                    $exits = ProductDetail::where('active', true)->where('product_id', $cart['product_id'])->where('batch_id',  $cart['batch_id']);
                        if($exits->count()==0){
                            $productDetail = new ProductDetail();
                            $productDetail->product_id = $cart['product_id'];
                            $productDetail->batch_id = $cart['batch_id'];
                            $productDetail->purchase_rate = $cart['purchase_rate'];
                            $productDetail->sale_rate = $cart['sale_rate'];
                            $productDetail->wholesale_rate = $cart['wholesale_rate'];
                            $productDetail->branch_id = app('BRANCHID');
                            $productDetail->created_by = app('USERID');
                            $productDetail->save();

                            $productInsetId = $productDetail->id;
                        }

                    // save purchase details
                    $purchaseDetail = new PurchaseDetail();
                    $purchaseDetail->purchase_id = $lastPurchaseId;
                    $purchaseDetail->entry_date = $purchase->entry_date;
                    $purchaseDetail->qty = $cart['qty'];
                    $purchaseDetail->product_detail_id = $exits->count()==0 ? $productInsetId : $exits->first()->id;
                    $purchaseDetail->branch_id = app('BRANCHID');
                    $purchaseDetail->created_by = app('USERID');
                    $purchaseDetail->save();

                    // save product transaction

                    $transaction = new ProductTransaction();
                    $transaction->invoice_id =  $lastPurchaseId;
                    $transaction->product_detail_id =  $exits->count()==0 ? $productInsetId : $exits->first()->id;
                    $transaction->transaction_type = 'purchase';
                    $transaction->qty = $cart['qty'];
                    $transaction->branch_id = app('BRANCHID');
                    $transaction->created_by = app('USERID');
                    $transaction->save();
                }

            }); #END transaction

            $res->message = 'Product was purchase';
            $res->status = 200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdatePurchase(Request $request)
    {
        
        // sometime needs product_details table update
        // purchase master table update
        // purchase details table update
        // transaction table update

        // dd($request->all());

        $res = new stdClass();

        try{
            DB::transaction(function () use($request) {

                // save purchase
                $purchase = $request->post('purchase_details');
                DB::table('purchases')
                    ->where('id', $purchase['id'])
                    ->update([
                    'supplier_id' => $purchase['supplier_id'],
                    'sub_total' => $purchase['sub_total'],
                    'discount_percent' => $purchase['discount_percent'],
                    'transport_cost' => $purchase['transport_cost'],
                    'other_cost' => $purchase['other_cost'],
                    'total' => $purchase['total'],
                    'payment_type' => $purchase['payment_type'],
                    'transaction_number' => $purchase['transaction_number'],
                    'paid' => $purchase['paid'],
                    'previous_due' => $purchase['previous_due'],
                    'note' => $purchase['note'],
                    'updated_by' => app('USERID')
                ]);

                // purchase details table update
                $purchaseDetail = $request->post('cart');
                foreach($purchaseDetail as $cart){
                    
                    // when change field value 
                    if($cart['id'] > 0){
                        
                        // update purchase details table
                        $details = PurchaseDetail::find($cart['id']);
                        $details->purchase_id = $purchase['id'];
                        $details->entry_date = $purchase['entry_date'];
                        $details->qty = $cart['qty'];
                        $details->product_detail_id = $cart['product_detail_id'];
                        $details->updated_by = app('USERID');
                        $details->save();

                        // update transaction table
                        $transaction = ProductTransaction::where('invoice_id', $cart['purchase_id'])
                                        ->where('product_detail_id', $cart['product_detail_id'])
                                        ->update([
                                            'qty' => $cart['qty'],
                                            'updated_by' => app('USERID')
                                        ]);

                    }

                    // this product not exists in purchase details table
                    if($cart['id'] == 0){
                        $exits = ProductDetail::where('active', true)->where('product_id', $cart['product_id'])
                                ->where('batch_id',  $cart['batch_id']);

                        if($exits->count()==0){
                            $productDetail = new ProductDetail();
                            $productDetail->product_id = $cart['product_id'];
                            $productDetail->batch_id = $cart['batch_id'];
                            $productDetail->purchase_rate = $cart['purchase_rate'];
                            $productDetail->sale_rate = $cart['sale_rate'];
                            $productDetail->wholesale_rate = $cart['wholesale_rate'];
                            $productDetail->branch_id = app('BRANCHID');
                            $productDetail->created_by = app('USERID');
                            $productDetail->save();

                            $productInsetId = $productDetail->id;
                        }

                        // save purchase details
                        $purchaseDetail = new PurchaseDetail();
                        $purchaseDetail->purchase_id = $purchase['id'];
                        $purchaseDetail->entry_date = $purchase['entry_date'];
                        $purchaseDetail->qty = $cart['qty'];
                        $purchaseDetail->product_detail_id = $exits->count()==0 ? $productInsetId : $exits->first()->id;
                        $purchaseDetail->branch_id = app('BRANCHID');
                        $purchaseDetail->created_by = app('USERID');
                        $purchaseDetail->save();

                        // save product transaction

                        $transaction = new ProductTransaction();
                        $transaction->invoice_id =  $purchase['id'];
                        $transaction->product_detail_id =  $exits->count()==0 ? $productInsetId : $exits->first()->id;
                        $transaction->transaction_type = 'purchase';
                        $transaction->qty = $cart['qty'];
                        $transaction->branch_id = app('BRANCHID');
                        $transaction->created_by = app('USERID');
                        $transaction->save();

                    }

                }
                // end purchase details

            }); #END transaction

            $res->message = 'Purchase was update';
            $res->status = 200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

    public function Delete(Request $request)
    {
        //
    }

    public function GetPurchaseInvoice()
    {
        $invoice = $this->purchaseRepo->NewPurchaseInvoice();
        return response()->json(['invoice' => $invoice]);
    }

    public function DeletePurchaseDetail(Request $request)
    {
        $res = new stdClass();
        try{
            DB::transaction(function () use($request) {
                // remove purchase details
                PurchaseDetail::where('id', $request->purchase_detail_id)
                ->delete();

                // remove product transaction
                ProductTransaction::where('invoice_id', $request->purchase_id)
                ->where('product_detail_id', $request->product_detail_id)
                ->delete();

                // remove product details if this not use other purchase details
                $countProduct = PurchaseDetail::where('active', true)
                        ->where('purchase_id','!=', $request->purchase_id)
                        ->where('product_detail_id', $request->product_detail_id)
                        ->count();
                if($countProduct == 0){
                    ProductDetail::where('id', $request->product_detail_id)->delete();
                } 
                
                // update purchase
                $purchase = Purchase::find($request->purchase_id);
                $purchase->sub_total -= $request->minus_amount;
                $purchase->save();
                
            }); #END transaction

            $res->message = 'Product remove successfully';
            $res->status = 200;

        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }
        
        return response()->json(['message' => $res->message], $res->status);
    }

    // reports

    public function PurchaseReport()
    {
        return view('purchase.report');
    }

    public function GetPurchaseReportWithoutDetails(Request $request)
    {

        $query = Purchase::with('Supplier')
                ->where('active', true)
                ->orderBy('id', 'desc')
                ->where('branch_id', app('BRANCHID'));
        
            if(isset($request->from_date) && isset($request->to_date) && $request->invoice == null){
                $query->whereBetween('entry_date', [$request->from_date, $request->to_date]);
            }

            if(isset($request->supplier_id) && $request->supplier_id != null){
                $query->where('supplier_id', $request->supplier_id);
            }

            if(isset($request->invoice) && $request->invoice != null){
                $query->where('invoice', $request->invoice);
            }

        $purchases = $query->get();
        return response()->json(['purchases' => $purchases]);
    }

    public function GetPurchaseReportWithDetails(Request $request)
    {
        
        $query = Purchase::with(['Supplier','PurchaseDetails.ProductDetail.Product.Unit','PurchaseDetails.ProductDetail.Batch', 'ProductTransactions'])
                ->where('active', true)
                ->orderBy('id', 'desc')
                ->where('branch_id', app('BRANCHID'));
        
            if(isset($request->from_date) && isset($request->to_date) && $request->invoice == null){
                $query->whereBetween('entry_date', [$request->from_date, $request->to_date]);
            }

            if(isset($request->supplier_id) && $request->supplier_id != null){
                $query->where('supplier_id', $request->supplier_id);
            }

            if(isset($request->invoice) && $request->invoice != null){
                $query->where('invoice', $request->invoice);
            }

        $purchases = $query->get();
        return response()->json(['purchases' => $purchases]);
    }

    public function GetSupplierWishInvoice(Request $request)
    {
        $invoices = Purchase::select('invoice')
                    ->where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->where('supplier_id', $request->supplier_id)
                    ->orderBy('id', 'desc')
                    ->get();

        return response()->json(['invoices' => $invoices]);
    }

    public function GetAllPurchaseInvoices()
    {
        $invoices = Purchase::select('invoice')
                    ->where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->orderBy('id', 'desc')
                    ->get();

        return response()->json(['invoices' => $invoices]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class ProductController extends Controller
{
    private $productRepo;
    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    public function Index()
    {
        return view('product.index');
    }

    public function GetProducts(Request $request)
    {
        $arg = '';
        if($request->input('pagination')) {
            $arg = $request->input('pagination');
        }

        $products = $this->productRepo->Get($arg);
        return response()->json(['products' => $products]);
    }

    public function Products()
    {
        return view('product.products');
    }

    public function ProductList()
    {
        return view('product.product_list');
    }

    Public function AddProduct(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:products|max:255',
            'unit_id' => 'required'
        ]);
        
        $res = $this->productRepo->InsertProduct($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdateProduct(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'unit_id' => 'required'
        ]);
        
        $res = $this->productRepo->Update($request);
        return response()->json(['message' => $res->message], $res->status);

    }

    public function DeleteProduct(Request $request)
    {
        $id = $request->id;
        $res = $this->productRepo->Delete($id);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function GetNewProductCode()
    {
        $productCode = $this->productRepo->NewProductCode();
        return response()->json(['code' => $productCode]);
    }
}

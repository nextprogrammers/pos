<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function PurchaseDetails()
    {
        return $this->hasMany('App\PurchaseDetail')->where('active', true);
    }
    
    public function ProductTransactions()
    {
        return $this->hasMany('App\ProductTransaction', 'invoice_id');
    }

    public function Supplier()
    {
        return $this->belongsTo('App\Supplier');
    }
}

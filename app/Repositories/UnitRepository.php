<?php
namespace App\Repositories;

use App\Unit;
use stdClass;

class UnitRepository {

    public function Get()
    {
        $units = Unit::where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->get();

        return $units;
    }

    public function GetById($unitId)
    {
        $unit = Unit::where('id', $unitId)
                    ->where('branch_id', app('BRANCHID'))
                    ->first();

        return $unit;
    }


    public function Insert($request)
    {
        $res = new stdClass();
        try{
            $unit = new Unit();
            $unit->name = $request->name;
            $unit->branch_id = app('BRANCHID');
            $unit->save();

            $res->message = 'Unit was add';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function Delete($unitId)
    {
        $res = new stdClass();
        try{
            $unit = $this->GetById($unitId);
            $unit->active = false;
            $unit->save();
            $res->message = "Unit was delete";
            $res->status = 200;
        }catch(\Exception $ex){
            $res->message = $ex->getMessage();
            $res->status = $ex->getCode();
        }

        return $res;
    }

    public function Update($request)
    {
        $res = new stdClass();
        try{
            $unit = Unit::find($request->id);
            $unit->name = $request->name;
            $unit->save();

            $res->message = 'Unit was update';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }
}
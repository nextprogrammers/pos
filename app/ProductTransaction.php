<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTransaction extends Model
{

    public function Purchase()
    {
        return $this->belongsTo('App\Purchase');
    }
}

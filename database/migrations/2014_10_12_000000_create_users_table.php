<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('username');
            $table->string('phone')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('branch_id');
            $table->boolean('active')->default(true);
            $table->boolean('is_developer')->default(false);
            $table->integer('saved_by');
            $table->rememberToken();
            $table->timestamps();
        });

        $user = new User();
        $user->name = "DevGroup";
        $user->phone = "01747160072";
        $user->email = "nextprogrammers@gmail.com";
        $user->username = "devGroup";
        $user->password = Hash::make(1);
        $user->branch_id = 1;
        $user->active = true;
        $user->is_developer = true;
        $user->saved_by = 1;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

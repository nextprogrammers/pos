<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleDetails extends Model
{
    public function Sale()
    {
        return $this->belongsTo('App\Sale');
    }
    
    public function ProductDetail()
    {
        return $this->belongsTo('App\ProductDetail');
    }
}

@extends('master')
@section('title', 'Customer Entry')
@section('breadcrumb', 'Customer Entry')
@section('page_name', 'Manage Customers')
@section('content')
<div id="app">
    <customer-component></customer-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


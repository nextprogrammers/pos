@extends('master')
@section('title', 'Unit Entry')
@section('breadcrumb', 'Unit Entry')
@section('page_name', 'Manage Units')
@section('content')
<div id="app">
    <unit-component></unit-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


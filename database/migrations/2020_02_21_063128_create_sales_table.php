<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->date('entry_date');
            $table->string('invoice');
            $table->string('sale_type');
            $table->decimal('sub_total',15,2);
            $table->float('discount_percent')->default(0);
            $table->float('tax_percent')->default(0);
            $table->float('vat_percent')->default(0);
            $table->decimal('other_cost',10,2);
            $table->decimal('total',15,2);
            $table->decimal('paid',15,2);
            $table->integer('payment_type')->default(1)->comment("1=> cash");
            $table->string('transaction_number')->nullable();
            $table->string('note')->nullable();
            $table->boolean('active')->default(true);
            $table->tinyInteger('branch_id');
            $table->tinyInteger('created_by');
            $table->tinyInteger('updated_by')->nullable();
            $table->tinyInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}

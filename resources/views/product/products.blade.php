@extends('master')
@section('title', 'Product Entry')
@section('breadcrumb', 'Product Entry')
@section('page_name', 'Manage Products')
@section('content')
<div id="app">
    <product-component></product-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


<?php
namespace App\Repositories;

use App\Batch;
use stdClass;

class BatchRepository {

    public function Get()
    {
        $batches = Batch::where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->get();

        return $batches;
    }

    public function GetById($batchId)
    {
        $batch = Batch::where('id', $batchId)
                    ->where('branch_id', app('BRANCHID'))
                    ->first();

        return $batch;
    }


    public function Insert($request)
    {
        $res = new stdClass();
        try{
            $batch = new Batch();
            $batch->name = $request->name;
            $batch->created_by = app('USERID'); 
            $batch->branch_id = app('BRANCHID');
            $batch->save();

            $res->message = 'Batch was add';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function Delete($batchId)
    {
        $res = new stdClass();
        try{
            $batch = $this->GetById($batchId);
            $batch->active = false;
            $batch->save();
            $res->message = "Batch was delete";
            $res->status = 200;
        }catch(Exception $ex){
            $res->message = $ex->getMessage();
            $res->status = $ex->getCode();
        }

        return $res;
    }

    public function Update($request)
    {
        $res = new stdClass();
        try{
            $batch = Batch::find($request->id);
            $batch->name = $request->name;
            $batch->save();

            $res->message = 'Batch was update';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }
}
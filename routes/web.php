<?php

Route::group(['middleware' => ['LoginCheck']], function () {

  // Main menu
  Route::get('home', "HomeController@Index");
  Route::get('product_module', "ProductController@Index");
  Route::get("dev_setting_module", "DevSetting@Index");
  

  // Product
  Route::get('products', "ProductController@Products");
  Route::get('get_product_code', "ProductController@GetNewProductCode");
  Route::post('add_product', "ProductController@AddProduct");
  Route::post('update_product', "ProductController@UpdateProduct");
  Route::post('get_products', "ProductController@GetProducts");
  Route::post('delete_product', "ProductController@DeleteProduct");

  // unit
  Route::get("units", "UnitController@Index");
  Route::get("get_units", "UnitController@GetUnits");
  Route::post("add_unit", "UnitController@AddUnit");

  // Purchase Module
  Route::get('purchase_module', 'PurchaseController@Index');

  // Customer
  Route::get('customers', 'CustomerController@Index');
  Route::get('get_customers', 'CustomerController@GetCustomers');
  Route::post('add_customer', 'CustomerController@AddCustomer');
  Route::post('update_customer', 'CustomerController@UpdateCustomer');
  Route::get('get_customer_code', 'CustomerController@GetCustomerNewCode');
  Route::post('delete_customer', 'CustomerController@DeleteCustomer');

  // supplier
  Route::get('suppliers', 'SupplierController@Index');
  Route::get('get_suppliers', 'SupplierController@GetSuppliers');
  Route::post('add_supplier', 'SupplierController@AddSupplier');
  Route::post('update_supplier', 'SupplierController@UpdateSupplier');
  Route::get('get_supplier_code', 'SupplierController@GetSupplierNewCode');
  Route::post('delete_supplier', 'SupplierController@DeleteSupplier');

  // purchase
  Route::get('purchase', 'PurchaseController@Purchase');
  Route::get('purchase/{id}', 'PurchaseController@Purchase');
  Route::post("get_purchase", "PurchaseController@GetPurchase");
  Route::get("get_purchase_invoice", "PurchaseController@GetPurchaseInvoice");
  Route::post("purchase_place", "PurchaseController@PurchasePlace");
  Route::post("get_purchase_by_id", "PurchaseController@GetPurchaseById");
  Route::post("delete_purchase_detail", "PurchaseController@DeletePurchaseDetail");
  Route::post("update_purchase", "PurchaseController@UpdatePurchase");
  Route::get("purchase_report", "PurchaseController@PurchaseReport");
  Route::post("get_purchase_report_without_details", "PurchaseController@GetPurchaseReportWithoutDetails");
  Route::post("get_purchase_report_with_details", "PurchaseController@GetPurchaseReportWithDetails");
  Route::get("get_all_purchase_invoice", "PurchaseController@GetAllPurchaseInvoices");


  // product details
  Route::post('product_exists', 'ProductDetailController@ProductExists');
  Route::post("exists_in_product_details", "ProductDetailController@ProductExists");
  Route::post("count_purchase_detail_product", "ProductDetailController@PurchaseDetailProductCheck");

  // sale 
  Route::get('sale', 'SaleController@Index');
  Route::get('get_sale_invoice', 'SaleController@GetSaleInvoice');
  Route::post('save_sale', 'SaleController@SaveSale');
  Route::get('sale/{id}', 'SaleController@Index');
  Route::post('get_sale_by_id', 'SaleController@GetSaleById');
  Route::post('delete_sale_detail', 'SaleController@DeleteSaleDetail');
  Route::post('update_sale', 'SaleController@UpdateSale');

  // batch 

  Route::get('get_batches', 'BatchController@GetBatches');
  Route::post('get_batch_by_product', 'BatchController@GetBatchesByProduct');

  // stock
  Route::post('get_stock', 'ProductDetailController@Stock');
  // DevSetting
  

  // Logout 
  Route::get("logout", "AuthController@Logout");
});

// Login process
Route::get("/", "AuthController@index");
Route::post("login", "AuthController@Login");

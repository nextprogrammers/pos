<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function Sale()
    {
        return $this->hasMany('App\Sale');
    }
}

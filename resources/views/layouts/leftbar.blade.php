<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="{{url('home')}}" style="color: #fff;font-size: 22px;text-transform: uppercase;line-height: 1.4">
                POS Solution
            </a>
        </div>  
    </div>

    <div class="sidebar-inner niceScrollleft">

        <div id="sidebar-menu">
            <ul>
                <!-- <li class="menu-title">Manage Your Shop </li> -->
                <li>
                    <a href="{{url('home')}}" class="waves-effect"><i class="ti-dashboard"></i><span> Dashboard </span></a>
                </li>
                 <li>
                    <a href="{{url('product_module')}}" class="waves-effect"><i class="ti-package"></i> <span> Products </span></a>
                </li>
                <li>
                    <a href="{{url('purchase_module')}}" class="waves-effect"><i class="ti-shopping-cart-full"></i> <span> Purchases </span></a>
                </li>
                <li>
                    <a href="{{url('sale_module')}}" class="waves-effect"><i class="typcn typcn-shopping-cart"></i> <span> Sales </span></a>
                </li>
                <li>
                    <a href="{{url('account_module')}}" class="waves-effect"><i class="far fa-money-bill-alt"></i> <span> Account </span></a>
                </li>
                <li>
                    <a href="{{url('hr_module')}}" class="waves-effect"><i class="ion-android-friends"></i> <span> HR & Payroll </span></a>
                </li>
                <li>
                    <a href="{{url('report_module')}}" class="waves-effect"><i class="ion-ios7-paper-outline"></i> <span> Reports </span></a>
                </li>
                <li>
                    <a href="{{url('administration_module')}}" class="waves-effect"><i class="ti-settings"></i> <span> Administration </span></a>
                </li>
                <li>
                    <a href="{{url('dev_setting_module')}}" class="waves-effect"><i class="ion-ios7-people-outline"></i> <span> Dev Settings </span></a>
                </li>
                <li>
                    <a href="{{url('logout')}}" class="waves-effect"><i class="typcn typcn-power-outline"></i> <span> Logout </span></a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-shopping-cart-full"></i><span> Purchase </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{url('suppliers')}}">Supplier</a></li>
                        <li><a href="{{url('purchase')}}">Purchase Entry</a></li>
                        <li><a href="{{url('purchase_report')}}">Purchase Report</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-shopping-cart-full"></i><span> Sale </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{url('sale')}}">Sale</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
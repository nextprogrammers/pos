<?php

namespace App\Http\Controllers;

use App\ProductDetail;
use App\ProductTransaction;
use App\PurchaseDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductDetailController extends Controller
{
   public function GetProductDetails()
   {
       $productDetails = ProductDetail::where('active', true)->get();
       return response()->json($productDetails, 200);
   }

   public function ProductExists(Request $request)
   {
    $product = ProductDetail::where('active', true)
                ->where('product_id', $request->product_id)
                ->where('batch_id', $request->batch_id);
                
    if($product->count() > 0){
        $product = $product->first();
    }else{
        $product = null;
    }

    return response()->json($product, 200);
   }

   public function PurchaseDetailProductCheck(Request $request)
   {
    $PurchaseDetail = PurchaseDetail::where('active', true)
                        ->where('product_detail_id', $request->product_detail_id)
                        ->count();
    return response()->json($PurchaseDetail, 200);
   }

   public function Stock(Request $request)
   {
       $arg =[];
        if(isset($request->productDetailId)){
            array_push($arg, ['t.product_detail_id', $request->productDetailId]);
        }

        // $products = DB::table('product_transactions')
        //             ->where('active', true)
        //             ->groupBy('product_detail_id')
        //             ->get();

        $products = DB::table('product_transactions as t')
                    ->join('product_details as d', 'd.id', '=', 't.product_detail_id')
                    ->join('products as p', 'p.id', '=', 'd.product_id')
                    ->select('t.product_detail_id', DB::raw('sum(t.qty) as stock'), 'p.name')
                    ->groupBy('t.product_detail_id')
                    ->where('t.active', true)
                    ->where($arg)
                    ->where('t.branch_id', app('BRANCHID'))
                    ->get();

        return response()->json($products);
   }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use App\User;
use Session;
use Symfony\Component\Routing\Route;

class AuthController extends Controller
{
    public function index()
    {
      if(Auth::check()){ return back();}
      return view('login');
    }

    public function Login(Request $request)
    {
      $validatedData = $request->validate([
         'username' => 'required',
         'password' => 'required',
      ]);

      $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/home');
        }
        return redirect()->back();
    }

    public function username()
    {
        return 'username';
    }

    public function Logout()
    {
        Session::flush();
        Auth::logout();
        return Redirect('/');
    }

}

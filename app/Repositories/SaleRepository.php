<?php
namespace App\Repositories;

use App\Sale;
use stdClass;

class SaleRepository {

    public function NewSaleInvoice() 
    {
        $sale = Sale::orderBy('id', 'desc');
        $saleId = ($sale->count() == 0 ? 0 : $sale->first()->id) + 1;
        $zeros = 5 - strlen($saleId);
        $saleInvoice = "S";
        for($i = 1; $i <= $zeros; $i++) $saleInvoice .= '0';
        $saleInvoice .= $saleId;
        return $saleInvoice;
    }
}
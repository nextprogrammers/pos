@extends('master')
@section('title', 'Supplier Entry')
@section('breadcrumb', 'Supplier Entry')
@section('page_name', 'Manage Suppliers')
@section("custom_css")
<link href="assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen">
@endsection
@section('content')
<div id="app">
    <supplier-component></supplier-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


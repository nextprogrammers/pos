<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    public function Purchase()
    {
        return $this->belongsTo('App\Purchase');
    }

    public function ProductDetail()
    {
        return $this->belongsTo('App\ProductDetail');
    }

    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public function ProductDetail()
    {
        return $this->hasMany('App\ProductDetail');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function Unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function ProductDetail()
    {
        return $this->hasMany('App\ProductDetail');
    }
}

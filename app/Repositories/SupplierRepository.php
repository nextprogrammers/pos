<?php
namespace App\Repositories;

use App\Supplier;
use stdClass;

class SupplierRepository {

    public function Get()
    {
        $suppliers = Supplier::where('active', true)
                    ->where('branch_id', app('BRANCHID'))
                    ->get()
                    ->map(function($supplier){
                        return[
                            'id' => $supplier->id,
                            'name' => $supplier->name,
                            'code' => $supplier->code,
                            'phone' => $supplier->phone,
                            'email' => $supplier->email,
                            'address' => $supplier->address,
                            'previous_due' => $supplier->previous_due,
                            'display_name' => $supplier->name.'-'.$supplier->phone.' ( '.$supplier->code.' )'
                        ];
                    });

        return $suppliers;
    }

    public function GetById($supplierId)
    {
        $supplier = Supplier::where('id', $supplierId)
                    ->where('branch_id', app('BRANCHID'))
                    ->first();

        return $supplier;
    }


    public function Insert($request)
    {
        $res = new stdClass();
        try{
            $supplier = new Supplier();
            $supplier->name = $request->name;
            $supplier->code = $this->NewSupplierCode();
            $supplier->phone = $request->phone;
            $supplier->email = $request->email;
            $supplier->previous_due = $request->previous_due;
            $supplier->address = $request->address;
            $supplier->active = true;
            $supplier->created_by = app('USERID'); 
            $supplier->branch_id = app('BRANCHID');
            $supplier->save();

            $res->message = 'Supplier was add';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function Delete($supplierId)
    {
        $res = new stdClass();
        try{
            $supplier = $this->GetById($supplierId);
            $supplier->active = false;
            $supplier->save();
            $res->message = "Supplier was delete";
            $res->status = 200;
        }catch(Exception $ex){
            $res->message = $ex->getMessage();
            $res->status = $ex->getCode();
        }

        return $res;
    }

    public function Update($request)
    {
        $res = new stdClass();
        try{
            $supplier = Supplier::find($request->id);
            $supplier->name = $request->name;
            $supplier->phone = $request->phone;
            $supplier->email = $request->email;
            $supplier->previous_due = $request->previous_due;
            $supplier->address = $request->address;
            $supplier->save();

            $res->message = 'Supplier was update';
            $res->status = 200;
        }catch(Exception $e){
            $res->message = $e->getMessage();
            $res->status =  $e->getCode();
        }

        return $res;
    }

    public function NewSupplierCode()
    {
        $supplier = Supplier::orderBy('id', 'desc');
        $supplierId = ($supplier->count() == 0 ? 0 : $supplier->first()->id) + 1;
        $zeros = 5 - strlen($supplierId);
        $newSupplierCode = "S";
        for($i = 1; $i <= $zeros; $i++) $newSupplierCode .= '0';
        $newSupplierCode .= $supplierId;
        return $newSupplierCode;
    }
}
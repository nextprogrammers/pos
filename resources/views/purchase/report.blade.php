@extends('master')
@section('title', 'Purchase Report')
@section('breadcrumb', 'Purchase Report')
@section('page_name', 'Purchases Report')
@section("custom_css")
<link href="assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen">
@endsection
@section('content')
<div id="app">
    <purchase-report-component></purchase-report-component>
</div>
<script src="{{mix('js/app.js')}}"></script>
@endsection


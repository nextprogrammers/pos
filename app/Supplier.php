<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function Purchase()
    {
        return $this->hasMany('App\Purchase');
    }
}

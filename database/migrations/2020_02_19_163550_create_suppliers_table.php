<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Supplier;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->float('previous_due')->default(0.0);
            $table->boolean('active')->default(true);
            $table->boolean('is_general')->default(false);
            $table->tinyInteger('branch_id');
            $table->tinyInteger('created_by');
            $table->tinyInteger('updated_by')->nullable();
            $table->tinyInteger('deleted_by')->nullable();
            $table->timestamps();
        });

        $supplier = new Supplier();
        $supplier->code = 'S00001';
        $supplier->name = 'General Supplier';
        $supplier->is_general = true;
        $supplier->branch_id = 1;
        $supplier->created_by = 1;
        $supplier->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}

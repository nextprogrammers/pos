<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('entry_date');
            $table->integer('supplier_id');
            $table->string('invoice');
            $table->double('sub_total', 15, 2);
            $table->float('vat_percent')->default(0.00);
            $table->float('discount_percent')->default(0.00);
            $table->float('transport_cost')->default(0.00);
            $table->float('other_cost')->default(0.00);
            $table->double('total', 15,2);
            $table->integer('payment_type')->default(1)->comment("1=> cash");
            $table->string('transaction_number')->nullable();
            $table->double('paid', 15, 2);
            $table->double('previous_due', 15, 2)->default(0.00);
            $table->string('note')->nullable();
            $table->integer('branch_id');
            $table->integer('active')->default(true);
            $table->tinyInteger('created_by');
            $table->tinyInteger('updated_by')->nullable();
            $table->tinyInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    public function PurchaseDetail()
    {
        return $this->hasMany('App\PurchaseDetail')->where('active', true);
    }

    public function Product()
    {
        return $this->belongsTo('App\Product');
    }

    public function Batch()
    {
        return $this->belongsTo('App\Batch');
    }

    public function SaleDetails()
    {
        return $this->hasMany('App\SaleDetails');
    }
    
}

require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
import UnitComponent from './components/UnitComponent.vue';
import ProductComponent from './components/ProductComponent.vue';
import CustomerComponent from './components/CustomerComponent.vue';
import SupplierComponent from './components/SupplierComponent.vue';
import PurchaseComponent from './components/PurchaseComponent.vue';
import PurchaseReportComponent from './components/PurchaseReportComponent.vue';
import SaleComponent from './components/SaleComponent.vue';

const app = new Vue({
    components: {
        UnitComponent,
        ProductComponent, 
        CustomerComponent, 
        SupplierComponent, 
        PurchaseComponent,
        PurchaseReportComponent,
        SaleComponent
    },
    el: '#app',
});

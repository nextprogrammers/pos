<?php

namespace App\Http\Controllers;

use App\Repositories\UnitRepository;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    private $unitRepo;
    public function __construct(UnitRepository $unitRepo)
    {
        $this->unitRepo = $unitRepo;
    }

    public function Index()
    {
        return view('product.units');
    }

    public function GetUnits()
    {
        $units = $this->unitRepo->Get();
        return response()->json(['units' => $units]);
    }

    public function AddUnit(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:units|max:255',
        ]);
        
        $res = $this->unitRepo->Insert($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function UpdateUnit(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:units|max:255',
        ]);
        
        $res = $this->unitRepo->Update($request);
        return response()->json(['message' => $res->message], $res->status);
    }

    public function DeleteUnit(Request $request)
    {
        $res = $this->unitRepo->Delete($request->input('unitId'));
        return response()->json(['message' => $res->message], $res->status);
    }

}
